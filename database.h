#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QFile>
#include <QList>

#include "question.h"
#include "answer.h"
#include "category.h"
#include "certificate.h"

#define DATABASE_FILENAME "questionBank.db3"

#include <QDebug>

class Database
{
public:
    Database();
    void init();
    Question getQuestionById(int id);
    QList<Question*> getRandomQuestions(int amount);
    QList<Question*> getRandomQuestionsByCertificateId(int amount, int certificateId);
    QList<Question*> getAllQuestions();
    int amountOfQuestionsByCertificateId(int certificateId);
    void addQuestion(Question question);
    QList<int> getAnswerIdsByQuestion(int id);
    int getCertificateIdByName(QString name);
    int getCategoryIdByName(QString name);
    QList<Category> getAllCategories();
    QList<Certificate> getAllCertificates();
    QStringList toStringList(QList<Certificate> certificates);
    QStringList toStringList(QList<Category> categories);
    QString getCertificateNameById(int id);
    QString getCategoryNameById(int id);
    void deleteQuestionById(int id);
private:
    void databaseSetup();
    QSqlDatabase db;
};

#endif // DATABASE_H
