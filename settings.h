#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QDebug>

#include "globalobject.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT
    
public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();
    
private:
    Ui::Settings *ui;
    QSettings* preferences;

private slots:
    void ok();
    void cancel();

};

#endif // SETTINGS_H
