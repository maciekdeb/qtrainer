#ifndef QUESTION_H
#define QUESTION_H

#include <QString>
#include <QList>

#include "answer.h"

class Question
{
public:
    Question();
    int questionId;
    QString questionContent;
    int correctAnswers;
    int category;
    int certType;
    QString explanation;
    QList<Answer*> answers;
};

#endif // QUESTION_H
