#ifndef TICKER_H
#define TICKER_H

#include <QThread>
#include <QTime>

#include <QDebug>

class Ticker : public QThread{
    Q_OBJECT
public:
    Ticker(QTime time, QObject *parent = 0);
private:
    void run() Q_DECL_OVERRIDE;
    QTime currentTime;

signals:
    void resultReady(const QString &s);
    void tick(QTime time);
};

#endif // TICKER_H
