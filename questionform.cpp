#include "questionform.h"
#include "ui_questionform.h"

QuestionForm::QuestionForm(int id,QWidget *parent) : QDialog(parent), ui(new Ui::QuestionForm){
    ui->setupUi(this);
    Database* database = globalObject.database;
    this->id = id;
    ui->comboBoxCertType->addItems(database->toStringList(database->getAllCertificates()));
    ui->comboBoxGroup->addItems(database->toStringList(database->getAllCategories()));
    if(id!=0){
        Question question = globalObject.database->getQuestionById(id);
        ui->plainTextEditQuestion->setPlainText(question.questionContent);
        ui->plainTextEditExplanation->setPlainText(question.explanation);
        ui->comboBoxCertType->setCurrentText(database->getCertificateNameById(question.certType));
        ui->comboBoxGroup->setCurrentText(database->getCategoryNameById(question.category));
        this->setWindowTitle("Edit question");
        foreach(Answer* answer, question.answers){
            addAnswer(answer->correct,answer->answerContent);
        }
    }
    else{
        this->setWindowTitle("Add question");
    }
    connect(ui->pushButtonAddAnswer,SIGNAL(clicked()),this,SLOT(addAnswer()));
    connect(ui->pushButtonOk,SIGNAL(clicked()),this,SLOT(ok()));
    connect(ui->pushButtonCancel,SIGNAL(clicked()),this,SLOT(cancel()));
}

QuestionForm::~QuestionForm(){
    delete ui;
}

void QuestionForm::addAnswer(){
    QChar sign = (char)65+i;
    QHBoxLayout* answerLayout = new QHBoxLayout();
    QCheckBox* answerCheckbox = new QCheckBox();
    QLabel* signLabel = new QLabel();
    QPlainTextEdit* plainEdit = new QPlainTextEdit();
    QPushButton* buttonRemoveAnswer = new QPushButton();
    buttonRemoveAnswer->setProperty("ANSWER_ID",QVariant(i));
    layoutAnswers.insert(i,answerLayout);
    layoutCheckbox.insert(i,answerCheckbox);
    layoutLabels.insert(i,signLabel);
    layoutPlainTextEdits.insert(i,plainEdit);
    QPixmap* pixmap1 = new QPixmap(":/icons/resources/remove.png");
    QIcon icon1(*pixmap1);
    buttonRemoveAnswer->setIcon(icon1);
    answerCheckbox->setObjectName("answerCheckbox");
    plainEdit->setObjectName("answerLineEdit");
    connect(buttonRemoveAnswer,SIGNAL(clicked()),this,SLOT(removeAnswer()));
    plainEdit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
    signLabel->setText("(" + QString(sign) + ")");
    answerLayout->addWidget(answerCheckbox);
    answerLayout->addWidget(signLabel);
    answerLayout->addWidget(plainEdit);
    answerLayout->addWidget(buttonRemoveAnswer);
    ui->verticalLayoutAnswersBox->addLayout(answerLayout);
    i++;
}

void QuestionForm::addAnswer(bool correct, QString answer){
    QChar sign = (char)65+i;
    QHBoxLayout* answerLayout = new QHBoxLayout();
    QCheckBox* answerCheckbox = new QCheckBox();
    QLabel* signLabel = new QLabel();
    signLabel->setObjectName("answerSignLabel");
    QPlainTextEdit* plainEdit = new QPlainTextEdit();
    QPushButton* buttonRemoveAnswer = new QPushButton();
    buttonRemoveAnswer->setProperty("ANSWER_ID",QVariant(i));
    layoutAnswers.insert(i,answerLayout);
    layoutCheckbox.insert(i,answerCheckbox);
    layoutLabels.insert(i,signLabel);
    layoutPlainTextEdits.insert(i,plainEdit);
    QPixmap* pixmap1 = new QPixmap(":/icons/resources/remove.png");
    QIcon icon1(*pixmap1);
    buttonRemoveAnswer->setIcon(icon1);
    answerCheckbox->setObjectName("answerCheckbox");
    plainEdit->setObjectName("answerLineEdit");
    connect(buttonRemoveAnswer,SIGNAL(clicked()),this,SLOT(removeAnswer()));
    plainEdit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
    signLabel->setText("(" + QString(sign) + ")");
    answerLayout->addWidget(answerCheckbox);
    answerLayout->addWidget(signLabel);
    answerLayout->addWidget(plainEdit);
    answerLayout->addWidget(buttonRemoveAnswer);
    ui->verticalLayoutAnswersBox->addLayout(answerLayout);
    i++;
    if(correct){
        answerCheckbox->setChecked(true);
    }
    plainEdit->setPlainText(answer);
}

void QuestionForm::removeAnswer(){
    QPushButton* sender = dynamic_cast<QPushButton*>(this->sender());
    int senderId = sender->property("ANSWER_ID").toInt();
    QHBoxLayout* answerLayout = layoutAnswers.take(senderId);
    layoutCheckbox.remove(i);
    layoutLabels.remove(i);
    layoutPlainTextEdits.remove(i);
    this->clearLayout(answerLayout->layout(),true);
}

void QuestionForm::clearLayout(QLayout* layout, bool deleteWidgets = true){
    while (QLayoutItem* item = layout->takeAt(0)){
        QWidget* widget;
        if (  (deleteWidgets)
              && (widget = item->widget())  ) {
            delete widget;

        }
        if (QLayout* childLayout = item->layout()) {
            clearLayout(childLayout, deleteWidgets);
        }
        delete item;
    }
}

void QuestionForm::ok(){
    Question question;  
    question.questionContent = ui->plainTextEditQuestion->toPlainText();
    question.explanation = ui->plainTextEditExplanation->toPlainText();
    int correctAnswers = 0;
    int order = 0;
    QMapIterator<int,QHBoxLayout*> it(layoutAnswers);
    while (it.hasNext()) {
        it.next();
        Answer* answer = new Answer();
        QHBoxLayout* layoutBoxAnswer = (QHBoxLayout*) it.value();
        QCheckBox* checkbox = layoutCheckbox.value(it.key());
        QLabel* label = layoutLabels.value(it.key());
        QPlainTextEdit* lineedit = layoutPlainTextEdits.value(it.key());
        QChar sign = label->text().at(1);
        QString reference = "%" + QString(sign) + "%";
        if(question.explanation.contains(reference)){
            question.explanation.replace(reference,"%" + QString(QChar(65+order)) + "%");
        }
        bool correct = checkbox->isChecked();
        if(correct){
            correctAnswers++;
        }
        answer->correct = correct;
        answer->answerContent = lineedit->toPlainText();
        question.answers.append(answer);
        order++;
    }
    question.correctAnswers = correctAnswers;
    if(validation(question)){

        if(id!=0){
            globalObject.database->deleteQuestionById(id);
        }
        if(!ui->comboBoxCertType->currentText().isEmpty()){
            question.certType =globalObject.database->getCertificateIdByName(ui->comboBoxCertType->currentText());
        }
        if(!ui->comboBoxGroup->currentText().isEmpty()){
            question.category =globalObject.database->getCategoryIdByName(ui->comboBoxGroup->currentText());
        }
        globalObject.database->addQuestion(question);
        this->close();
    }
}

void QuestionForm::cancel(){
    this->close();
}

bool QuestionForm::validation(Question question){
    bool result = true;
    if(question.answers.size()==0){
        QMessageBox::critical(this,"Error","Question must have at least one answer");
        result = false;
    }
    else if(question.correctAnswers==0){
        QMessageBox::critical(this,"Error","Question must have at least one correct answer");
        result = false;
    }
    else if(ui->comboBoxCertType->currentText().trimmed().isEmpty()){
        QMessageBox::critical(this,"Error","Certification type cannot be empty");
        result = false;
    }
    else if(ui->comboBoxGroup->currentText().trimmed().isEmpty()){
        QMessageBox::critical(this,"Error","Question category cannot be empty");
        result = false;
    }
    return result;
}
