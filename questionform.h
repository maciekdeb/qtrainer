#ifndef QUESTIONFORM_H
#define QUESTIONFORM_H

#include <QDialog>
#include <QCheckBox>
#include <QLineEdit>
#include <QMap>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPlainTextEdit>

#include "question.h"
#include "globalobject.h"

#include <QDebug>

namespace Ui {
class QuestionForm;
}

class QuestionForm : public QDialog
{
    Q_OBJECT
    
public:
    explicit QuestionForm(int id,QWidget *parent = 0);
    ~QuestionForm();
    
private:
    Ui::QuestionForm *ui;
    int i = 0;
    QMap<int,QHBoxLayout*> layoutAnswers;
    QMap<int,QLabel*> layoutLabels;
    QMap<int,QCheckBox*> layoutCheckbox;
    QMap<int,QPlainTextEdit*> layoutPlainTextEdits;
    void clearLayout(QLayout* layout, bool deleteWidgets);
    bool validation(Question question);
    int id;

private slots:
    void addAnswer();
    void addAnswer(bool correct, QString answer);
    void removeAnswer();
    void ok();
    void cancel();
};

#endif // QUESTIONFORM_H
