# QTrainer #

Sefl-study software used for training. Written in Qt.

### Features ###

* Two modes training and exams
* Multiselect questions
* Question bank editor

### License ###

MIT

### Screenshots ###

![screen1.png](https://bitbucket.org/repo/aezar6/images/2110775585-screen1.png)