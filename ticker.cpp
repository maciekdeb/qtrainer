#include "ticker.h"

Ticker::Ticker(QTime time, QObject *parent) : QThread(parent){
    this->currentTime = time;
}

void Ticker::run(){
    while((currentTime!=(QTime(0,0,0)))&&currentTime.isValid()){
        currentTime = currentTime.addSecs(-1);
        QThread::sleep(1);
        emit tick(currentTime);
    }
    emit resultReady("TIME ELAPSED");
}
