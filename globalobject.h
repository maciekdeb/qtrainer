#ifndef GLOBALOBJECT_H
#define GLOBALOBJECT_H

#include "database.h"

class GlobalObject
{
public:
    GlobalObject();
    Database* database;
};

extern GlobalObject globalObject;

#endif // GLOBALOBJECT_H
