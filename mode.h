#ifndef MODE_H
#define MODE_H

enum mode{
    EXAM,
    EXAM_RESULTS,
    BROWSE,
    BROWSE_ANSWERS,
    NONE
};

#endif // MODE_H
